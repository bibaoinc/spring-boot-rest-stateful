create table call_limit (
	id varchar2(10) primary key,
	limit number(3),
	group_ind char(1)
);

insert into call_limit values('A1', 2, 'N');

insert into call_limit values('A2', 3, 'N');

insert into call_limit values('G', 4, 'Y');