package com.bibao.boot.springbootreststateful;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages="com.bibao.boot")
@EnableJpaRepositories(basePackages = {"com.bibao.boot.repository"})
@EntityScan(basePackages = {"com.bibao.boot.entity"})
public class SpringBootRestStatefulApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootRestStatefulApplication.class, args);
	}

}

